@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Appointments</h1>
        <form method="GET" action="{{ route('appointments.index') }}">

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Doctor</label>

                <div class="col-md-6">
                    <label>
                        <select name="doctor_id" class="form-control" id="doctor_id">
                            <option value="">Select doctor</option>
                            @foreach($doctors as $doctorId => $name)
                                <option value="{{$doctorId}}" {{request('doctor_id') == $doctorId ? 'selected' : ''}}>{{$name}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">Date</label>

                <div class="col-md-6">
                    <label>
                        <select name="booked_at" class="form-control" id="booked_at">
                            <option value="">Select Date</option>
                            @foreach($bookedAt as $booked)
                                <option value="{{$booked}}" {{request('booked_at') == $booked ? 'selected' : ''}}>{{$booked}}</option>
                            @endforeach
                        </select>
                    </label>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" id="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>


        <div class="container">
            <h2>Appontment details</h2>
            <p id="message"></p>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Start time</th>
                    <th>Specialty name</th>
                    <th>Patient name</th>
                </tr>
                </thead>
                <tbody>
                @foreach($appointments as $appointment)
                    <tr>
                        <td>{{$appointment->booked_at}}</td>
                        <td>{{$appointment->start_time}}</td>
                        <td>{{$appointment->specialty->name}}</td>
                        <td>{{$appointment->patient->name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $("#doctor_id").change(function(){
                let doctorId = $('#doctor_id').val();
                $.getJSON('api/appointment-exists?doctor_id='+doctorId, function(result){
                    if (!result.appointmentsExists) {
                        $('#booked_at').attr("disabled", true);
                        $('#submit').attr("disabled", true);
                        $('#message').text('This doctor does not have any appointments')
                    } else {
                        $('#submit').attr("disabled", false);
                        $('#booked_at').attr("disabled", false);
                    }
                });
            });
        });
    </script>
@endsection