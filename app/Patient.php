<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'name',
        'date_of_birth',
        'sex',
    ];

    public function setDateOfBirthAttribute($value): void
    {
        $this->attributes['date_of_birth'] = Carbon::parse($value)->format('Y-m-d');
    }
}
