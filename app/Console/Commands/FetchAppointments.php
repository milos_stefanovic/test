<?php

namespace App\Console\Commands;

use App\Services\AppointmentService;
use Illuminate\Console\Command;

class FetchAppointments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:appointments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches appointments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param AppointmentService $service
     * @return mixed
     */
    public function handle(AppointmentService $service)
    {
        $service->saveAppointments();
    }
}
