<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Doctor;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $appointments = Appointment::getFiltered(\request()->all(), (bool)auth()->user());
        $doctors = Doctor::pluck('name', 'id');
        $bookedAt = Appointment::distinct()->pluck('booked_at');

        return view('appointments.index', compact('appointments', 'doctors', 'bookedAt'));
    }
}
