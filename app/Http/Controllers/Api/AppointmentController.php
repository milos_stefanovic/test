<?php

namespace App\Http\Controllers\Api;

use App\Appointment;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class AppointmentController extends Controller
{
    /**
     * @return JsonResponse
     * @throws ValidationException
     */
    public function appointmentExists(): JsonResponse
    {
        $this->validate(\request(), [
            'doctor_id' => 'required|integer'
        ]);

        $doctorId = \request('doctor_id');
        $appointmentsExists = Appointment::filter(['doctor_id' => $doctorId], (bool)auth()->user())->exists();

        return response()->json(['appointmentsExists' => $appointmentsExists]);
    }
}
