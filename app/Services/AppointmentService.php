<?php


namespace App\Services;


use App\Appointment;
use App\Clinic;
use App\Doctor;
use App\Patient;
use App\Speciality;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class AppointmentService
{
    /** @noinspection PhpComposerExtensionStubsInspection */
    public function fetchAppointments(): \SimpleXMLElement
    {
        $client = new Client();
        $uri = 'http://ch-api-test.herokuapp.com/xml';
        $headers = [
            'auth' => ['Myah', '654321'],
        ];

        $response = $client->request('GET', $uri, $headers);

        return simplexml_load_string($response->getBody());
    }

    public function saveAppointments()
    {
        DB::transaction(function () {

            foreach ($this->fetchAppointments() as $appointment) {

                $doctor = Doctor::firstOrCreate([
                    'name' => $appointment->doctor->name
                ]);

                $clinic = Clinic::firstOrCreate([
                    'name' => $appointment->clinic->name
                ]);

                $speciality = Speciality::firstOrCreate([
                    'name' => $appointment->specialty->name
                ]);

                $patient = Patient::firstOrCreate([
                    'name' => $appointment->patient->name,
                    'date_of_birth' => $appointment->patient->date_of_birth,
                    'sex' => $appointment->patient->sex,
                ]);

                Appointment::create([
                    'start_date' => $appointment->start_date,
                    'start_time' => $appointment->start_time,
                    'booked_at' => $appointment->booked_at,
                    'clinic_id' => $clinic->id,
                    'doctor_id' => $doctor->id,
                    'patient_id' => $patient->id,
                    'specialty_id' => $speciality->id,
                ]);
            }
        }, 3);

    }
}