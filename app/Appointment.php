<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Appointment
 * @package App
 */
class Appointment extends Model
{
    protected $fillable = [
        'booked_at',
        'start_time',
        'start_date',
        'clinic_id',
        'patient_id',
        'doctor_id',
        'specialty_id',
    ];

    /**
     * @param array $all
     * @param bool $loggedIn
     * @return mixed
     */
    public static function getFiltered(array $all, bool $loggedIn)
    {
        return static::with(['specialty', 'patient'])
            ->filter($all, $loggedIn)
            ->get();
    }

    /**
     * @param $value
     */
    public function setBookedAtAttribute($value): void
    {
        $this->attributes['booked_at'] = Carbon::parse($value)->toDateTimeString();
    }

    /**
     * @param $value
     */
    public function setStartTimeAttribute($value): void
    {
        $this->attributes['start_time'] = Carbon::parse($value)->toTimeString();
    }

    /**
     * @param $value
     */
    public function setStartDateAttribute($value): void
    {
        $this->attributes['start_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    /**
     * @return BelongsTo
     */
    public function clinic(): BelongsTo
    {
        return $this->belongsTo(Clinic::class);
    }

    /**
     * @return BelongsTo
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * @return BelongsTo
     */
    public function specialty(): BelongsTo
    {
        return $this->belongsTo(Speciality::class);
    }

    /**
     * @return BelongsTo
     */
    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class);
    }

    /**
     * @param $query
     * @param array $filter
     * @param bool $loggedIn
     * @return mixed
     */
    public function scopeFilter($query, array $filter, bool $loggedIn = true)
    {
        if ($filter['booked_at'] ?? null) {
            $query->where('booked_at', $filter['booked_at']);
        }

        if ($filter['doctor_id'] ?? null) {
            $doctorId = $filter['doctor_id'];
            $query->whereHas('doctor', function ($query) use ($doctorId) {
                $query->where('id', $doctorId);
            });
        }
        if (!$loggedIn) {
            $query->whereHas('patient', function ($query) {
                $query->where('date_of_birth', '<', Carbon::now()->subYears(18)->toDateTimeString());
            });
        }

        return $query;
    }
}
